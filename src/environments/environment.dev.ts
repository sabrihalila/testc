export const environment = {
  production: false,
  CATEGORIE_BASE_URL: 'http://localhost:8000/wp-json/wp/v2/categories',
  POST_BASE_URL: 'http://localhost:8000/wp-json/wp/v2/posts',
  POST_ID_CAT: 'http://localhost:8000/wp-json/wp/v2/posts?categories=',
  POST_ID: 'http://localhost:8000/wp-json/wp/v2/posts?include=',
  COM_POST: 'http://localhost:8000/wp-json/wp/v2/comments?post=',
  ADDCOMMENT: 'http://localhost:8000/wp-json/wp/v2/comments',

};

