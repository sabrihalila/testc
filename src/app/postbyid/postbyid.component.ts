import {Component, OnInit} from '@angular/core';
import {PostService} from "../services/post.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-postbyid',
  templateUrl: './postbyid.component.html',
  styleUrls: ['./postbyid.component.css']
})
export class PostbyidComponent implements OnInit {
  catId: string = '';
  postDetails: any;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private postService: PostService) {
  }

  ngOnInit() {


    this.activatedRoute.params.subscribe(data => {
      this.catId = data.id;

    });

    if (this.catId) {

      this.postService.getPostbycat(this.catId).subscribe(data => {
        this.postDetails = data;
        console.log(this.postDetails);
      });
    }

  }

}
