import { Component, OnInit } from '@angular/core';
import {PostService} from "../services/post.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  postResult: any;
  postList: any;
  constructor(private postService: PostService ,  private router: Router) { }

  ngOnInit(): void {
    this.getPostList();
  }
  getPostList() {
    this.postService.getPost().subscribe((data: any[]) => {
      this.postResult = data;
      this.postList = this.postResult;
      console.log(this.postList);
    });
  }
}
