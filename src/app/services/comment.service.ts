import {Injectable} from '@angular/core';
import {HttpHeaders, HttpClient} from '@angular/common/http';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  constructor(private httpClient: HttpClient, private Headers: HttpClient, private router: Router) {
  }

  addComm(name: string, comment: string, post_id: number, email: string) {

    console.log(comment);
    console.log(post_id);
    return this.httpClient.post(
      'http://localhost:8000/wp-json/wp/v2/comments',
      {post: post_id, author_name: name, content: comment, author_email: email},).subscribe(data => {

    });
    
  }
}
