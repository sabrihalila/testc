import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../environments/environment.dev';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategorieService {
  constructor(private httpClient: HttpClient) { }


  getCategorie(){
    let url = environment.CATEGORIE_BASE_URL;
    return this.httpClient.get(url);
  }



}
