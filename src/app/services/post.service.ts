import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment.dev";

@Injectable({
  providedIn: 'root'
})
export class PostService {
  constructor(private httpClient: HttpClient) {
  }


  getPost() {
    let url = environment.POST_BASE_URL;
    return this.httpClient.get(url);
  }
  getPostbycat(id_cat) {
    let url = environment.POST_ID_CAT+id_cat;
    return this.httpClient.get(url);
  }
  getPostbyID(id) {
    let url = environment.POST_ID+id;
    return this.httpClient.get(url);
  }

  getcommentby(idpost) {
    let url = environment.COM_POST+idpost;
    return this.httpClient.get(url);
  }
}
