import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PostComponent} from "./post/post.component";
import {PostbyidComponent} from "./postbyid/postbyid.component";
import {GetpostComponent} from "./getpost/getpost.component";


const routes: Routes = [
  {path: '', component: PostComponent },
  {path: 'post/:id', component: PostbyidComponent },
  {path: 'postdet/:id', component: GetpostComponent },
  ]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
