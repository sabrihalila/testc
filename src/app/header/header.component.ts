import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {CategorieService} from "../services/categorie.service";


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  categorieResult: any;
  categorieList: any;
  categorieId: string = '';
  constructor(private categorieService: CategorieService ,  private router: Router) { }

  ngOnInit(): void {
    this.getCategorieList();
  }
  getCategorieList() {
    this.categorieService.getCategorie().subscribe((data: any[]) => {
      this.categorieResult = data;
      this.categorieList = this.categorieResult;
      console.log(this.categorieList);
    });
  }



}
