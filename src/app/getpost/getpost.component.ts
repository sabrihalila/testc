import {Component, OnInit} from '@angular/core';
import {PostService} from "../services/post.service";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommentService} from "../services/comment.service";
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {HttpHeaders} from "@angular/common/http";
import {validate} from "codelyzer/walkerFactory/walkerFn";

@Component({
  selector: 'app-getpost',
  templateUrl: './getpost.component.html',
  styleUrls: ['./getpost.component.css']
})
export class GetpostComponent implements OnInit {
  Id: string = '';
  postDetails: any;
  commD: any;
  addcommForm: FormGroup;
  loading = false;
  errorMessage: string;

  constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private router: Router, private postService: PostService, private commentservice: CommentService,) {
  }

  ngOnInit() {

    this.activatedRoute.params.subscribe(data => {
      this.Id = data.id;

    });

    if (this.Id) {

      this.postService.getPostbyID(this.Id).subscribe(data => {
        this.postDetails = data;
        console.log(this.postDetails);
      });

      this.postService.getcommentby(this.Id).subscribe(data => {
        this.commD = data;
        console.log(this.commD);
      });
    }
    this.addcommForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      comment: ['', [Validators.required]],
      email: [null, [Validators.email]],
    });

  }

  reloadCurrentPage() {

  }

  onadd(post_id) {
    this.loading = true;
    const name = this.addcommForm.get('name').value;
    const comment = this.addcommForm.get('comment').value;
    const email = this.addcommForm.get('email').value;
    this.commentservice.addComm(name, comment, post_id, email);

    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate(['/postdet/'+post_id]);
    });
  }

}

